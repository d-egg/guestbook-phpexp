Archiv von dem guestbookDW75 Projekt (PHP exploration/toy project)

# Benutzung

Install docker

    make up

Ports:
- 8888 guestbook
- 8889 phpmyadmin

Tests ausführen:

    make test

Der coverage report ist in `output/coverage/index.html`.

Herunterfahren (in einem anderen Terminal):

    make down
