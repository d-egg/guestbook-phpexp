<?php
/*
 * Geteilter header für alle HTML Seiten
 */
  require_once('__init__.php');

  /* Start a session on every page */

  $cookieParams = [
    0,      // lifetime (0)
    '/',    // path ('/')
    '',     // domain, ('')
    False,  // secure (False)
    False,  // httponly (False)
  ];

  session_set_cookie_params(...$cookieParams);
  session_start();

  /* Session Initialization */
  if (!isset($_SESSION['postsperpage'])) {
    $_SESSION['postsperpage'] = '2';
  }

  /* Error Output Formatting
   *
   * Für die Fehler-Anzeige in ungültigen POST requests
   */

  function outputGenericErrorMessages_cond($formErrors) {
    $errorStrings = [];
    foreach($formErrors as $fieldName => $failedConstraints) {
      $fieldErrors = [];
      foreach($failedConstraints as $constraint) {
        $fieldErrors[] = $constraint->getGenericErrorMessage();
      }
      if ($fieldErrors) {
        $errorStrings[$fieldName] = $fieldErrors;
      }
    }
    if (empty($errorStrings)) {
      return;
    }
    echo '<div class="errorout">';
    echo "<h3>Fehler</h3>\n";
    foreach($errorStrings as $fieldName => $errorMessages) {
      echo "<p>" . htmlentities($fieldName) . "</p>\n";
      echo "<ul>\n";
      foreach($errorMessages as $error) {
        echo "<li>" . htmlentities($error) . "</li>\n";
      }
      echo "</ul>\n";
    }
    echo '</div>';
  }

  /*
   * Convert To Number
   *
   * Um Zahlen in GET requests zu erzwingen.
   */

  function convertToNumber($origin, $str) {
    if (!ctype_digit($str)) {
      die("Invalid request in $origin.  Not a number: " . htmlentities($str));
    }
    return (int)$str;
  }

  /*
   * Current Values
   *
   * Bei fehlerhaften POST requests sollen die aktuellen Werte erhalten bleiben.
   * Erstelle die entsprechenden value Attribute für forms
   */

  function getCurrentValue_attr($allPostKeys) {
    $currentValues = [];
    foreach($allPostKeys as $post_key) {
      $post_value = $_REQUEST[$post_key] ?? '';
      $currentValues[$post_key] = ' value="' . htmlentities($post_value) . '" ';
    }
    return $currentValues;
  }

  /*
   * Query String Formatter
   *
   * Create a URL query string from one or more key/value mappings
   */
  function getQueryString(...$kv_mappings) {
    $encodedParts = [];
    foreach ($kv_mappings as $kvm) {
      foreach ($kvm as $key => $value) {
        $encodedParts[] = $key . "=" . urlencode($value);
      }
    }
    return '?' . join('&', $encodedParts);
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Guestbook DW75</title>
    <meta charset="UTF-8">
    <link href="css/guestbook.css" rel="stylesheet" type="text/css">
  </head>
  <body>

  <div id="body-column">

    <header>

        <!-- ============ banner ============ -->
        <!-- book image link: https://pixabay.com/en/book-education-books-reference-25155/ -->

        <div id="banner-bg">
            <div id="banner">
                <img id="bookhead" src="img/book-25155_640.png" alt="bookhead">
            </div>

            <span id="theme" class="bookfont bookcolor">Gästebuch DW75</span>
        </div>

        <!-- ============ Navigation ============ -->

        <nav>
            <input type="checkbox" id="mobile-menu">
            <label for="mobile-menu">&#9776;Menü</label>
            <ul>
                <!-- vermeide Abstand zwischen den inline-block boxen (fixme) -->
                <li><a href="/views/guestbook.php" title="Home">Home</a></li><li><a href="/views/user.php" title="User">User</a></li><li><a href="" title="Dummy">...</a></li>
            </ul>
        </nav>

    </header>
    <main>

        <h1>Hallo, <?php echo $_SESSION['loggedinas'] ?? 'Gast' ?></h1>
