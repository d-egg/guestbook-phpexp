<?php
/*
 * Die user login/logout/register HTML Seite
 */
  require_once('__init__.php');
  require_once('models/register.php');

  require_once('views/header.php');

  function printLogoutCard() {

    /*
    * Logout
    */
    $logouhere = <<<EOD
    <article class="row col col-100">\n
    <h3>Logout</h3>\n

    <form action="/views/user.php" method="post" id="logout_form">
      <button type="submit" name="action" value="logout">Ausloggen</button>
    </form>

    </article>

EOD;

    echo $logouhere;
  }


  function printLoginCard($currentValues, $login_errors) {

    /*
    * Login
    */
    $loginhere = <<<EOD
    <article class="row col col-100">\n
    <h3>Login</h3>\n

    <form action="/views/user.php" method="post" id="login_form">

      <table>
      <tr>
        <td><label for="login_nick_f">Nutzer:</label></td>
        <td><input id="login_nick_f" type="text" name="login_nick"
            {$currentValues['login_nick']}
            placeholder="nick" maxlength="30"></td>
      </tr>

      <tr>
        <td><label for="login_pass_f">Passwort:</label></td>
        <td><input id="login_pass_f" type="password" name="login_pass"
            {$currentValues['login_pass']}
            placeholder="Passwort" maxlength="30"></td>
      </tr>
      </table>

      <button type="submit" name="action" value="login">Einloggen</button>
    </form>

    </article>
EOD;

    echo $loginhere;
    outputGenericErrorMessages_cond($login_errors);
  }

  function printRegistrationCard($currentValues, $registration_errors) {
    /*
    * Register
    */
    $registerhere = <<<EOD
    <article class="row col col-100">
    <h3>Registrierung</h3>
    <form action="/views/user.php" method="post" id="register_form">
      <table>
      <tr>
        <td><label for="reg_nick_f">Nutzer:</label></td>
        <td><input id="reg_nick_f" type="text" name="reg_nick"
            {$currentValues['reg_nick']}
            placeholder="nick" maxlength="30"></td>
      </tr>

      <tr>
      <td><label for="reg_pass_f">Passwort:</label></td>
      <td><input id="reg_pass_f" type="password" name="reg_pass"
          {$currentValues['reg_pass']}
          placeholder="Passwort" maxlength="30"></td>
      </tr>

      <tr>
      <td><label for="reg_firstname_f">Vorname:</label></td>
      <td><input id="reg_firstname_f" type="text" name="reg_firstname"
          {$currentValues['reg_firstname']}
          placeholder="Vorname" maxlength="30"></td>
      </tr>

      <tr>
      <td><label for="reg_lastname_f">Nachname:</label></td>
      <td><input id="reg_lastname_f" type="text" name="reg_lastname"
          {$currentValues['reg_lastname']}
          placeholder="Nachname" maxlength="30"></td>
      </tr>

      <tr>
      <td><label for="reg_email_f">Email:</label></td>
      <td><input id="reg_email_f" type="email" name="reg_email"
          {$currentValues['reg_email']}
          placeholder="you@the.net" maxlength="30"></td>
      </tr>
      </table>

      <button type="submit" name="action" value="register">Registrieren</button>
    </form>

    </article>
EOD;

    echo $registerhere;
    outputGenericErrorMessages_cond($registration_errors);
  }

  // =======================

  /*
   * This page talks to the UserRegistry class in the register module
   */
  $userReg = new \register\UserRegistry();

  /*
   * value attributes for this page
   */
  $allFormFields = [
    'login_nick',
    'login_pass',
    'reg_nick',
    'reg_pass',
    'reg_firstname',
    'reg_lastname',
    'reg_email',
  ];
  $currentValues = getCurrentValue_attr($allFormFields);

  /*
   * no error defaults
   */

  $login_errors = [];
  $registration_errors = [];

  /*
   * Action Processing
   */
  $postAction = $_POST['action'] ?? '';
  if ($postAction == 'logout') {
    $_SESSION['loggedinas'] = null;
    header('Location: /views/guestbook.php');

  } elseif ($postAction == 'login') {
    $login_errors = $userReg->attemptToLogin(
        $_POST['login_nick'] ?? '',
        $_POST['login_pass'] ?? '');

    if (empty($login_errors)) {
      $_SESSION['loggedinas'] = $_POST['login_nick'];
      header('Location: /views/guestbook.php');
    }
  } elseif ($postAction == 'register') {
    $registration_errors = $userReg->registerNewUser([
      'nick' => $_POST['reg_nick'] ?? '',
      'password' => $_POST['reg_pass'] ?? '',
      'vorname' => $_POST['reg_firstname'] ?? '',
      'nachname' => $_POST['reg_lastname'] ?? '',
      'email' => $_POST['reg_email'] ?? '',
    ]);
    if (empty($registration_errors)) {
      $_SESSION['loggedinas'] = trim($_POST['reg_nick']);
      header('Location: /views/guestbook.php');
    }
  }

  /*
   * If no post errors, display all parts.  Otherwise focus the card with the
   * errors.
   */

  if (empty($login_errors) && empty($registration_errors)) {
    if (isset($_SESSION['loggedinas'])) {
      printLogoutCard();
    }
    printLoginCard($currentValues, $login_errors);
    printRegistrationCard($currentValues, $registration_errors);
  } elseif (!empty($login_errors)) {
    printLoginCard($currentValues, $login_errors);
  } elseif (!empty($registration_errors)) {
    printRegistrationCard($currentValues, $registration_errors);
  }

  // ============================

  /*
   * User base
   */
  echo "<hr>";

  function outputUserTable() {
    echo '<article class="row col col-100 userbase">';
    $userReg = new \register\UserRegistry();
    $isFirstPass = True;
    echo "<h3>User Base:</h3>\n";
    echo "<table>\n";
    foreach($userReg->getAllUserData() as $userRecord) {
      unset($userRecord['password']);
      if ($isFirstPass) {
        $isFirstPass = False;
        echo "<tr>\n";
        foreach (array_keys($userRecord) as $key) {
          $ucKey = ucwords($key);
          echo "<th>$ucKey</th>\n";
        }
        echo "</tr>\n";
      }

      echo "<tr>\n";
      foreach ($userRecord as $value) {
        echo "<td>" . htmlentities($value) . "</td>\n";
      }
      echo "</tr>\n";

    }
    echo "</table></article>\n";
  }

  outputUserTable();

  require_once('views/footer.php');
