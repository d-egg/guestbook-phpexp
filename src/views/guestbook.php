<?php
/*
 * Die Gästebuch Hauptseite
 */
  require_once('__init__.php');
  require_once('models/messages.php');

  require_once('views/header.php');

  /*
   * This page talks to the Post class in the messages module
   */
  $postOffice = new \messages\Posts();

  /*
   * value attributes for this page
   */
  $allFormFields = [
    'message',
  ];
  $currentValues = getCurrentValue_attr($allFormFields);

  /*
   * Message display configuration
   *
   * - how many posts per page
   * - which page
   * - which post when displaying comments
   */

  // per request refpost
  $refpost = $_REQUEST['refpost'] ?? null;
  if (!is_null($refpost)) {
    $refpost = convertToNumber('refpost', $refpost);
  }

  // per-request page
  $page = convertToNumber('page', $_REQUEST['page'] ?? '1');

  // ppp setting is stored in the session
  $postsPerPage = $_REQUEST['postsperpage'] ?? null;
  if (!is_null($postsPerPage)) {
    $postsPerPage = convertToNumber('postsperpage', $postsPerPage);
    $_SESSION['postsperpage'] = $postsPerPage;
  } else {
    $postsPerPage = $_SESSION['postsperpage'];
  }

  /*
   * Action processing
   */
  $message_errors = [];

  $postAction = $_POST['action'] ?? '';
  if ($postAction == 'message') {
    $formData = [];
    $formData['user'] = $_SESSION['loggedinas'] ?? 'Anonymous';
    $formData['message'] = $_POST['message'] ?? '';

    $queryRefpost = '';
    if (is_null($refpost)) {
      $message_errors = $postOffice->storeNewPost($formData);
    } else {
      $queryRefpost = "?refpost=$refpost";
      $formData['refpost'] = $refpost;
      $message_errors = $postOffice->storeNewComment($formData);
    }

    // redirect to the new message
    if (empty($message_errors)) {
      header('Location: /views/guestbook.php' . $queryRefpost);
    }
  }

  /*
   * Display Referenced Post
   */

  if (!is_null($refpost)) {
    $mainpost = $postOffice->getReferencedPost($refpost);

    $nutzer_esc = htmlentities($mainpost["nutzer"]);
    $datum_esc = htmlentities($mainpost["datum"]);
    $message_esc = htmlentities($mainpost["message"]);

    $mainposthere = <<<EOD
<article class="row col col-100 messagebox">
<div class="messageheadmain">{$nutzer_esc} {$datum_esc}
</div>
<div class="messagemain">{$message_esc}</div>
</article>
EOD;

    echo $mainposthere;
  }

  /*
   * Obtain message data from the postOffice
   */

  if (is_null($refpost)) {
    $messages = $postOffice->getPostsForPage($postsPerPage, $page);
    $numberOfPages = $postOffice->getNumberOfPostPages($postsPerPage);
  } else {
    $messages = $postOffice->getCommentsForPage($postsPerPage, $page, $refpost);
    $numberOfPages = $postOffice->getNumberOfCommentsPages($postsPerPage,
        $refpost);
  }

  /*
   * Create the page navigation
   */

  $nextPageLink = '<span class="pagearrow">' . htmlentities('>') . '</span>';
  $prevPageLink = '<span class="pagearrow">' . htmlentities('<') . '</span>';

  $refpostQuery = [];
  if (!is_null($refpost)) {
    $refpostQuery['refpost'] = $refpost;
  }
  if ($page < $numberOfPages) {
    $queryString = getQueryString(
        $refpostQuery,
        ['page' => $page + 1]);
    $nextPageLink = "<a href=\"{$queryString}\" title=\"next page\">"
        . $nextPageLink . "</a>";
  }
  if ($page > 1) {
    $queryString = getQueryString(
        $refpostQuery,
        ['page' => $page - 1]);
    $prevPageLink = "<a href=\"{$queryString}\" title=\"prev page\">"
        . $prevPageLink . "</a>";
  }

  $paginationhere = <<<EOD
<div class="pagination monospacetext">
{$prevPageLink} {$page}/{$numberOfPages} {$nextPageLink}
</div>
EOD;

  echo $paginationhere;

  /*
   * Create the message display
   */

  foreach ($messages as $post) {
    $commentsLink = '';
    if (is_null($refpost)) {
      $numberOfComments = $postOffice->getNumberOfCommentsPages(1, $post['id']);
      $queryString = getQueryString(
          ['refpost' => $post['id']]);
      $commentsLink = "<a href=\"{$queryString}\" title=\"comments\">"
          . "Kommentare(" . $numberOfComments . ")</a>";
    }

    $nutzer_esc = htmlentities($post["nutzer"]);
    $datum_esc = htmlentities($post["datum"]);
    $message_esc = htmlentities($post["message"]);

    $messagehere = <<<EOD
<article class="row col col-100 messagebox">
<div class="messagehead">{$nutzer_esc} {$datum_esc} {$commentsLink}
</div>
<div class="message">{$message_esc}</div>
</article>
EOD;

    echo $messagehere;

  }

  /*
   * Message box to contribute a new post or comment
   */

  $postOrComment = "Neuer Beitrag";
  $refpostAttr = '';
  if (!is_null($refpost)) {
    $postOrComment = "Neuer Kommentar";
    $refpostAttr = ' <input type="hidden" value="' . htmlentities($refpost)
      . '" name="refpost" /> ';
  }
  $currentText = htmlentities($_POST['message'] ?? '');

  $posthere = <<<EOD
<article class="row col col-100">\n
<h3>$postOrComment</h3>\n

<form action="/views/guestbook.php" method="post" id="message_form">

  <textarea id="message_f" type="text" name="message"
      placeholder="Tippen Sie hier...">{$currentText}</textarea>
  {$refpostAttr}<br>
  <button type="submit" name="action" value="message">Senden</button>
</form>

</article>
EOD;

  echo $posthere;
  outputGenericErrorMessages_cond($message_errors);

  /*
   * Adjust the page count
   */

  $ppphere = <<<EOD
<article class="row col col-100">
<h3>Meldungen Pro Seite</h3>

<form action="/views/guestbook.php" method="post" id="ppp_form">

  <input type="number" name="postsperpage" min="1" max="9"
      value="$postsPerPage">
  {$refpostAttr}

  <button type="submit" name="action" value="ppp">Zeigen</button>
</form>

</article>
EOD;

  echo $ppphere;

  // footer
  require_once('views/footer.php');
