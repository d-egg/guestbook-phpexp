<?php
namespace formdata;

require_once('formdata.php');
require_once('models/constraints.php');

use PHPUnit\Framework\TestCase;
use constraints;

class FormProcessor_cloneTemplate extends TestCase {

  function testReturnValueIsADeepCopy() {

    $nestedClass = new class {
      function __construct() { $this->arr = ['a', 'org', 'r']; }
    };
    $recursiveStructure = [
      'f1' => [
          'i1' => $nestedClass,
      ],
    ];

    $fp = new FormProcessor();
    $fieldInfo = $fp->cloneTemplate($recursiveStructure);
    $fieldInfo['f1']['i1']->arr[1] = 'mod';
    $this->assertEquals('mod', $fieldInfo['f1']['i1']->arr[1]);
    $this->assertEquals('org', $recursiveStructure['f1']['i1']->arr[1]);
  }
}

/*
 * validateFormData is tested through UserRegistry::validateRegistrationData()
 *
 * (There is no time)
 */
