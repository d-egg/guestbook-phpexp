<?php
namespace register;

require_once('register.php');
require_once('models/constraints.php');
require_once('models/formdata.php');

use PHPUnit\Framework\TestCase;
use constraints;
use formdata;


/*
 * UsernameRegistry Tests
 */

class UserRegistry_Constructor extends TestCase {
  function testConstructorExecutesWithoutError() {
    $ur = new UserRegistry();
    $this->assertTrue(isset($ur->registryTemplate));
  }
}

class UserRegistry_registerNewUser extends TestCase {

  function getUrWithRegTemplate($structureSpec) {

    $this->srStubBad = $this->createMock(constraints\ServerConstraint::class);
    $this->srStubBad->method('isError')
         ->willReturn(True);
    $this->srStubGood = $this->createMock(constraints\ServerConstraint::class);
    $this->srStubGood->method('isError')
         ->willReturn(False);
    $this->srStubGood->method('isFatal')
         ->willReturn(False);

    $regTemplate = [];

    foreach($structureSpec as $fieldname => $isGoodBools) {
      $constraintMocks = [];
      foreach ($isGoodBools as $isGood) {
        if ($isGood) {
          $constraintMocks[] = clone $this->srStubGood;
        } else {
          $constraintMocks[] = clone $this->srStubBad;
        }
      }
      $regTemplate[$fieldname] = $constraintMocks;
    }

    return new class($regTemplate) extends UserRegistry {
      function __construct($rt) {$this->registryTemplate = $rt;}
    };
  }

  function testBasicGoodPath() {
    $ur = $this->getUrWithRegTemplate([
      'f1' => [True, False, False],
      'f2' => [True],
      'f3' => [False],
    ]);
    $formData = [
      'f1' => 'v1',
      'f2' => 'v2',
      'f3' => 'v3',
    ];
    $response = $ur->validateRegistrationData($formData);
    $this->assertEquals(['f1', 'f3'], array_keys($response));
    $this->assertEquals(2, count($response['f1']));
    $this->assertEquals(1, count($response['f3']));
  }

  function testAllErrorResponsesAreErrors() {
    $ur = $this->getUrWithRegTemplate([
      'f1' => [True, False, False, True, False],
    ]);
    $formData = [
      'f1' => 'v1',
    ];
    $response = $ur->validateRegistrationData($formData);
    $this->assertEquals(['f1'], array_keys($response));
    $this->assertEquals(3, count($response['f1']));
    foreach ($response['f1'] as $constraint) {
      $this->assertTrue($constraint->isError());
    }
  }

  function testEmptyResponseWhenNoErrors() {
    $ur = $this->getUrWithRegTemplate([
      'f1' => [True, True, True],
      'f2' => [True, True],
      'f3' => [True],
    ]);
    $formData = [
      'f1' => 'v1',
      'f2' => 'v2',
      'f3' => 'v3',
    ];
    $response = $ur->validateRegistrationData($formData);
    $this->assertEmpty($response);
  }

  function testUnsupportedFieldsPresentAsServerErrors() {
    $ur = $this->getUrWithRegTemplate([
      'f3' => [True],
    ]);
    $formData = [
      'f1' => 'v1',
      'f2' => 'v2',
      'f3' => 'v3',
    ];
    $response = $ur->validateRegistrationData($formData);
    $se = formData\FormProcessor::SERVER_ERROR_FIELD_NAME;
    $this->assertEquals([$se], array_keys($response));
    $this->assertEquals(2, count($response[$se]));
  }

  function testFatalTestAbortsForViolatedConstraints() {
    $ur = $this->getUrWithRegTemplate([
      'f1' => [True, False, False, False],
    ]);
    $formData = [
      'f1' => 'v1',
    ];
    $ur->registryTemplate['f1'][1]->method('isFatal')
                                  ->willReturn(True);
    $response = $ur->validateRegistrationData($formData);
    $this->assertEquals(['f1'], array_keys($response));
    $this->assertEquals(1, count($response['f1']));
  }

  function testFatalTestWontAbortForGoodConstraints() {
    $ur = $this->getUrWithRegTemplate([
      'f1' => [True, False, False, False],
    ]);
    $formData = [
      'f1' => 'v1',
    ];
    $ur->registryTemplate['f1'][0]->method('isFatal')
                                  ->willReturn(True);
    $response = $ur->validateRegistrationData($formData);
    $this->assertEquals(['f1'], array_keys($response));
    $this->assertEquals(3, count($response['f1']));
  }
}

class UserRegistry_getFieldInfo extends TestCase {

  function testReturnValueIsADeepCopy() {

    $nestedClass = new class {
      function __construct() { $this->arr = ['a', 'org', 'r']; }
    };
    $recursiveStructure = [
      'f1' => [
          'i1' => $nestedClass,
      ],
    ];
    $ur = new class($recursiveStructure) extends UserRegistry {
      function __construct($rt) { $this->registryTemplate = $rt; }
    };

    $fieldInfo = $ur->getFieldInfo();
    $fieldInfo['f1']['i1']->arr[1] = 'mod';
    $this->assertEquals('mod', $fieldInfo['f1']['i1']->arr[1]);
    $this->assertEquals('org', $ur->registryTemplate['f1']['i1']->arr[1]);
  }
}
