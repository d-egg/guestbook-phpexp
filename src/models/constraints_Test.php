<?php
namespace constraints;

require_once('constraints.php');
require_once('models/dbtesthelper.php');

use PHPUnit\Framework\TestCase;


/*
 * Test helper classes
 */

class InvalidCharsConstraint_RegexFake extends InvalidCharsConstraint {
  function __construct($fakeRegex) {
    $this->fakeRegex = $fakeRegex;
  }
  function _getAllowedCharsRegex() {
    return $this->fakeRegex;
  }
}


/*
 * CharDescription Tests
 */

class CharDescriptionTest extends TestCase {

  function testCorrectInitialization() {
    $cd = new CharDescription("ü", True);
    $this->assertEquals("ü", $cd->charString);
    $this->assertEquals(True, $cd->isValid);
    $this->assertEquals(2, $cd->getByteLength());
  }

  function testGivesCorrectUnicodeName() {
    $cd = new CharDescription("ü", 7);
    $this->assertEquals("LATIN SMALL LETTER U WITH DIAERESIS",
        $cd->getUnicodeName());
  }
}


/*
 * CharAnalysis Tests
 */

class CharAnalysis_IsValidUsernameTest extends TestCase {

  function testEmptyAnalysisRecordIfNotValid() {
    $analysis = new CharAnalysis([]);
    $this->assertFalse($analysis->isValidUsername());
  }
  function testValidUsername() {
    $cdValid = new CharDescription('a', True);
    $analysis = new CharAnalysis(
      [$cdValid, $cdValid, $cdValid, $cdValid]);
    $this->assertTrue($analysis->isValidUsername());
  }
  function testInvalidUsername() {
    $cdValid = new CharDescription('a', True);
    $cdInvalid = new CharDescription('7', False);
    $analysis = new CharAnalysis(
      [$cdValid, $cdValid, $cdInvalid, $cdValid]);
    $this->assertFalse($analysis->isValidUsername());
  }
}

class CharAnalysis_GetInvalidElementsTest extends TestCase {
  function testInvalidUsername() {
    $cdValid = new CharDescription('a', True);
    $cdInvalid = new CharDescription('7', False);
    $analysis = new CharAnalysis(
      [$cdInvalid, $cdValid, $cdValid, $cdInvalid, $cdInvalid]);
    $actual = array_keys($analysis->getInvalidElements());
    $this->assertEquals([0, 3, 4], $actual);
  }
}


/*
 * ServerConstraint Tests
 */

class Constraint_ServerTest extends TestCase {

  function testMessageGenerationWithoutError() {
    $sr = new ServerConstraint();
    $sr->setError("any message");
    $this->assertGreaterThan(5, strlen($sr->getGenericErrorMessage()));
  }
}

/*
 * LengthLimitsConstraint Tests
 */

class Constraint_LengthLimitsTest extends TestCase {

  function testMessageGenerationWithoutError() {
    $sr = new LengthLimitsConstraint(1, 2);
    $sr->setError(3);
    $this->assertGreaterThan(5, strlen($sr->getGenericErrorMessage()));
  }

  function inclusiveRangeProvider() {
    return [
      [3, 5, 123, False],
      [3, 5, 12345, False],
      [3, 5, 12, True],
      [3, 5, 123456, True],
    ];
  }
  /**
   * @dataProvider inclusiveRangeProvider
   */
  function testAllowsOnlyInclusiveRange($min, $max, $str, $isError) {
    $cd = new LengthLimitsConstraint(3, 5);
    $cd->verify($str);
    $this->assertEquals($isError, $cd->isError());
  }

  function invalidRangeProvider() {
    return [[-1, 0], [0, -1], [-5, -7]];
  }
  /**
   * @expectedException constraints\ConstraintsLogicException
   * @dataProvider invalidRangeProvider
   */
  function testNegativeMinMaxThrowsLogicError($min, $max) {
    $cd = new LengthLimitsConstraint($min, $max);
    $cd->verify('');
  }
}


/*
 * InvalidCharsConstraint Tests
 */

class Constraint_InvalidCharsTest extends TestCase {

  function testMessageGenerationWithoutError() {
    $uaStub = $this->createMock(CharAnalysis::class);
    $uaStub->method('getInvalidElements')
           ->willReturn([2 => 0, 4 => 0, 5 => 0]);
    $sr = new InvalidCharsConstraint(["letters" => "\p{L}"]);
    $sr->setError($uaStub);
    $this->assertGreaterThan(5, strlen($sr->getGenericErrorMessage()));
  }
}

class InvalidCharsConstraint_verifyTest extends TestCase {

  function testVerifySetsErrorCorrectly() {
    // No idea how you selectively stub a method.  What a mockery!
    $uaStub = $this->createMock(CharAnalysis::class);
    $uaStub->method('isValidUsername')
           ->willReturn(False);
    $sr = new class($uaStub) extends InvalidCharsConstraint {
      function __construct($uaStub) {
        parent::__construct('invalid');
        $this->uaStub = $uaStub;
      }
      function _getCharAnalysis($s) { return $this->uaStub; }
    };
    $sr->verify('');
    $this->assertEquals($uaStub, $sr->characterAnalysis);
  }
}

class InvalidCharsConstraint_getAllowedCharsRegex extends TestCase {

  function allowedUsernameConfigProvider() {
    return [
      [[1 => 'aü', 2 => 'üX', 3 => '$a'], 'aü|üX|$a'],
      [[1 => 'öb', 2 => 'ä', 3 => '$&/'], 'öb|ä|$&/'],
    ];
  }

  /**
   * @dataProvider allowedUsernameConfigProvider
   */
  function testArbitraryByteStringsGetConcatAsIs($config, $expected) {
    $sr = new InvalidCharsConstraint($config);
    $this->assertEquals($expected, $sr->_getAllowedCharsRegex());
  }

  function testEmptyConfigGetsSwallowed() {
    $sr = new InvalidCharsConstraint([1 => 'öb', 2 => '', 3 => '$&/']);
    $expected = 'öb|$&/';
    $actual = $sr->_getAllowedCharsRegex();
    $this->assertEquals($expected, $actual);
  }
}

class InvalidCharsConstraint_isAllowedChar extends TestCase {

  function testMatchesSelectedSingleChars() {

    $sr = new InvalidCharsConstraint_RegexFake('\p{L}');

    $this->assertTrue($sr->_isAllowedChar('a'));
    $this->assertTrue($sr->_isAllowedChar('ü'));

    $this->assertFalse($sr->_isAllowedChar('2'));
    $this->assertFalse($sr->_isAllowedChar('%'));
    $this->assertFalse($sr->_isAllowedChar(' '));
  }

  function stringLengthsNotEqualToOne() {
    return [['ab'], ['%a'], ['abba'], ['%"$'], [''], ['  ']];
  }
  /**
   * @dataProvider stringLengthsNotEqualToOne
   * @expectedException constraints\ConstraintsLogicException
   */
  function testThrowsForInvalidLength($subject) {
    $sr = new InvalidCharsConstraint([]);
    $sr->_isAllowedChar($subject);
  }
}

class InvalidCharsConstraint_GetCharAnalysis extends TestCase {

  private $allowedCharsConfig = [
      "L" => "\p{L}",
      "N" => "\p{N}",
  ];

  function testBasicGoodPath() {
    $sr = new InvalidCharsConstraint($this->allowedCharsConfig);
    $analysis = $sr->_getCharAnalysis('üö%7');
    $expected = [
      1 => True,
      2 => True,
      3 => False,
      4 => True,
    ];

    $actual = [];
    foreach ($analysis as $position => $charDesc) {
      $actual[$position] = $charDesc->isValid;
    }
    $this->assertEquals($expected, $actual);
  }

  /**
   * @expectedException constraints\ConstraintsLogicException
   */
  function testThrowsForInvalidInput() {
    $sr = new InvalidCharsConstraint($this->allowedCharsConfig);
    $sr->_getCharAnalysis('ü'[0]);
  }
}


/*
 * CharDiversityConstraint Tests
 */

class Constraint_CharDiversityTest extends TestCase {

  private $mandatoryCG = [
    "lc" => "abcdefghijklmnopqrstuvxyz",
    "uc" => "ABCDEFGHIJKLMNOPQRSTUVXYZ",
    "digit" => "1234567890",
  ];

  function testMessageGenerationWithoutError() {
    $sr = new CharDiversityConstraint(["lc" => "abc"]);
    $sr->setError(["letters", "numbers"]);
    $this->assertGreaterThan(5, strlen($sr->getGenericErrorMessage()));
  }

  function testValidWhenAllGroupsPresent() {
    $sr = new CharDiversityConstraint($this->mandatoryCG);
    $sr->verify('dE3');
    $this->assertFalse($sr->isError());
    $this->assertFalse((bool)$sr->missingCharGroups);
  }

  function testSelectedMissingCharGroupsPresentInReturnValue() {
    $sr = new CharDiversityConstraint($this->mandatoryCG);
    $sr->verify('dE');
    $this->assertTrue($sr->isError());
    $this->assertEquals(['digit'], $sr->missingCharGroups);
  }

  function testAllCharGroupsPresentInReturnValue() {
    $sr = new CharDiversityConstraint($this->mandatoryCG);
    $sr->verify('');
    $this->assertTrue($sr->isError());
    $this->assertEquals(['lc', 'uc', 'digit'], $sr->missingCharGroups);
  }
}

class Constraint_InvalidWordsConstraint extends TestCase {

  private $invalidWords = ['yuge', 'hombre', 'bad'];

  function testFailedConstraintWhenInvalidWordsAppear() {
    $sr = new InvalidWordsConstraint($this->invalidWords);
    $sr->verify('Hombres coming. Bad');
    $this->assertTrue($sr->isError());
    $this->assertEquals(['bad', 'hombre'], $sr->wordViolations);
  }

  function testValidConstraintWhenNoInvalidWordsAppear() {
    $sr = new InvalidWordsConstraint($this->invalidWords);
    $sr->verify('Individuals without borders');
    $this->assertFalse($sr->isError());
  }
}

class Constraint_InvalidEmailConstraint extends TestCase {

  function testFailedConstraintWhenInvalidEmailAppears() {
    $sr = new InvalidEmailConstraint();
    $sr->verify('this@cant@be.right');
    $this->assertTrue($sr->isError());
  }

  function testValidConstraintWhenValidEmailAppears() {
    $sr = new InvalidEmailConstraint();
    $sr->verify('okay@example.com');
    $this->assertFalse($sr->isError());
  }
}

// =========================================

class Constraint_DBNickConstraint extends \dbaccess\DBTransactionsTestHelper {

  function testNonerrorConstraintIfNickPresentAsRequired() {
    $sr = new DBNickConstraint(True);
    $sr->verify('sweeper');
    $this->assertFalse($sr->isError());
  }
  function testErrorConstraintIfNickPresentAsProhibited() {
    $sr = new DBNickConstraint(False);
    $sr->verify('sweeper');
    $this->assertTrue($sr->isError());
    $this->assertRegExp('/bereits/', $sr->getGenericErrorMessage());
  }
  function testNonerrorConstraintIfNickAbsentAsRequired() {
    $sr = new DBNickConstraint(False);
    $sr->verify('broom');
    $this->assertFalse($sr->isError());
  }
  function testErrorConstraintIfNickAbsentAsProhibited() {
    $sr = new DBNickConstraint(True);
    $sr->verify('broom');
    $this->assertTrue($sr->isError());
    $this->assertRegExp('/nicht/', $sr->getGenericErrorMessage());
  }
}

class Constraint_DBRefpost extends \dbaccess\DBTransactionsTestHelper {

  function testNonerrorConstraintIfRefpostExists() {
    $sr = new DBRefpostExistenceConstraint();
    $sr->verify(' 1 ');
    $this->assertFalse($sr->isError());
  }
  function testErrorConstraintIfRefpostNonexistant() {
    $sr = new DBRefpostExistenceConstraint();
    $sr->verify('100000');
    $this->assertTrue($sr->isError());
    $this->assertRegExp('/100000/', $sr->getGenericErrorMessage());
  }
}
