<?php
/*
 * register module
 *
 * Behandelt alles was mit login, logout, und Registrierung zu tun hat.  Die
 * Anforderungen an Form-Daten werden mit constraints konfiguriert.
 */
namespace register;

require_once('__init__.php');
require_once('models/constraints.php');
require_once('models/dbaccess.php');
require_once('models/formdata.php');

use Exception;
use constraints;
use dbaccess;
use formdata;


/*
 * Exceptions: Errors and Info
 */

class UserRegistryException extends Exception {}
class UserRegistryLogicException extends UserRegistryException {}
class UserRegistryVerficationFail extends UserRegistryException {}


/*
 * UserRegistry
 *
 * The main class. It handles registration requests.
 */

class UserRegistry {

  // These are the known form fields used in registration request/response
  const PROFILETRAIT_USERNAME = "nick";
  const PROFILETRAIT_PASSWORD = "password";
  const PROFILETRAIT_FNAME = "vorname";
  const PROFILETRAIT_LNAME = "nachname";
  const PROFILETRAIT_EMAIL = "email";

  // as long as there is no field hierarchy...
  const FIELDCONSTRAINT_LENGTH = "length";
  const FIELDCONSTRAINT_CHARDIV = "diversity";
  const FIELDCONSTRAINT_INVCHAR = "invchar";
  const FIELDCONSTRAINT_INVCOMM = "invcomment";
  const FIELDCONSTRAINT_EMAIL = "email";
  const FIELDCONSTRAINT_NEWNICK = "newnick";

  // character groups that have to be present in a password
  protected $mandatoryCharGroups = [
    "Kleinbuchstaben" => "abcdefghijklmnopqrstuvxyz",
    "Großbuchstaben" => "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
    "Zahlen" => "1234567890",
    "Sonderzeichen" => "$%&!#*+?/(){}=",
  ];

  // min/max ranges for fields
  protected $minMaxLengths = [
    UserRegistry::PROFILETRAIT_USERNAME => ['min' => 3, 'max' => 30],
    UserRegistry::PROFILETRAIT_PASSWORD => ['min' => 8, 'max' => 30],
  ];

  // map descriptive terms to single-character-match regular-expressions.
  protected $allowedNickCharGroups = [
      "letters" => "\p{L}",
      "number" => "\p{N}",
      "hyphen" => "[-]",
      "underscore" => "[_]",
  ];

  protected $allowedNameCharGroups = [
      "letters" => "\p{L}",
      "number" => "\p{N}",
      "space" => "[ °'_-]",
  ];

  // ---

  function __construct() {
    $this->db = new dbaccess\Datenbank();
    $this->registryTemplate = $this->_getRegistryTemplate();
  }

  function getFieldInfo() {
    $formProc = new formdata\FormProcessor();
    return $formProc->cloneTemplate($this->registryTemplate);
  }

  function validateRegistrationData($formData) {
    $formProc = new formdata\FormProcessor();
    $registrationErrors = $formProc->validateFormData(
        $formData,
        $this->registryTemplate);
    return $registrationErrors;
  }

  function registerNewUser($formData) {
    $registrationErrors = $this->validateRegistrationData($formData);
    if (!empty($registrationErrors)) {
      return $registrationErrors;
    }
    $this->db->storeNewUser(
      $formData['nick'],
      password_hash($formData['password'], PASSWORD_DEFAULT),
      $formData['vorname'],
      $formData['nachname'],
      $formData['email']);
    return $registrationErrors;
  }

  // ---

  function attemptToLogin($nick_evil, $password_evil) {
    // Nicht der richtige Ort für ein Constraint, aber im Moment werden
    // constraints über mehrere Felder nicht unterstützt.

    // Ein Fehler für alle Fehler.  Nicht preisgeben ob es den nick gibt, zB.
    $sr = new constraints\ServerConstraint();
    $sr->setError("Kein gültiges login.");
    $login_errors = ['_servererror' => ['login-error' => $sr]];

    $pwhash = $this->db->getPasswordHash($nick_evil);
    if (is_null($pwhash)) {
      return $login_errors;
    } elseif (!password_verify($password_evil, $pwhash)) {
      return $login_errors;
    } else {
      return [];
    }
  }

  function getAllUserData() {
    $allUserData = [];
    foreach($this->db->getAllNicks() as $nick) {
      $allUserData[] = $this->db->getUserData($nick);
    }
    return $allUserData;
  }

  // ---

  function _getRegistryTemplate() {

    // username
    $userLength = $this->minMaxLengths[self::PROFILETRAIT_USERNAME];
    $userLengthConstraint = new constraints\LengthLimitsConstraint(
            $userLength['min'], $userLength['max']);
    $userLengthConstraint->setFatal(True);

    $userInvCharConstraint = new constraints\InvalidCharsConstraint(
            $this->allowedNickCharGroups);

    $userNewNickConstraint = new constraints\DBNickConstraint(False);

    $usernameField = [];
    $usernameField[self::FIELDCONSTRAINT_LENGTH] = $userLengthConstraint;
    $usernameField[self::FIELDCONSTRAINT_INVCHAR] = $userInvCharConstraint;
    $usernameField[self::FIELDCONSTRAINT_NEWNICK] = $userNewNickConstraint;

    // password
    $pwLenght = $this->minMaxLengths[self::PROFILETRAIT_PASSWORD];
    $pwLengthConstraint =  new constraints\LengthLimitsConstraint(
            $pwLenght['min'], $pwLenght['max']);
    $pwLengthConstraint->setFatal(True);

    $pwCharDivConstraint = new constraints\CharDiversityConstraint(
            $this->mandatoryCharGroups);

    $passwordField = [];
    $passwordField[self::FIELDCONSTRAINT_LENGTH] = $pwLengthConstraint;
    $passwordField[self::FIELDCONSTRAINT_CHARDIV] = $pwCharDivConstraint;

    // ===

    $firstnameLengthC = new constraints\LengthLimitsConstraint(1, 100);
    $firstnameLengthC->setFatal(True);
    $firstnameInvCharC = new constraints\InvalidCharsConstraint(
            $this->allowedNameCharGroups);

    $firstnameField = [];
    $firstnameField[self::FIELDCONSTRAINT_LENGTH] = $firstnameLengthC;
    $firstnameField[self::FIELDCONSTRAINT_INVCOMM] = $firstnameInvCharC;

    // ---

    $lastnameLengthC = new constraints\LengthLimitsConstraint(1, 100);
    $lastnameLengthC->setFatal(True);
    $lastnameInvCharC = new constraints\InvalidCharsConstraint(
            $this->allowedNameCharGroups);

    $lastnameField = [];
    $lastnameField[self::FIELDCONSTRAINT_LENGTH] = $lastnameLengthC;
    $lastnameField[self::FIELDCONSTRAINT_INVCOMM] = $lastnameInvCharC;

    // ---

    $emailLengthC = new constraints\LengthLimitsConstraint(1, 100);
    $emailLengthC->setFatal(True);
    $emailValidityC = new constraints\InvalidEmailConstraint();

    $emailField = [];
    $emailField[self::FIELDCONSTRAINT_LENGTH] = $emailLengthC;
    $emailField[self::FIELDCONSTRAINT_EMAIL] = $emailValidityC;

    // ===

    // registry template
    $registryTemplate = [];
    $registryTemplate[self::PROFILETRAIT_USERNAME] = $usernameField;
    $registryTemplate[self::PROFILETRAIT_PASSWORD] = $passwordField;
    $registryTemplate[self::PROFILETRAIT_FNAME] = $firstnameField;
    $registryTemplate[self::PROFILETRAIT_LNAME] = $lastnameField;
    $registryTemplate[self::PROFILETRAIT_EMAIL] = $emailField;

    return $registryTemplate;
  }
}
