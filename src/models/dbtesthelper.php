<?php
/*
 * testhelper module
 *
 * Nich ideal, aber wenn alle DB-Tests diese TestCase Unterklasse nutzen wird
 * eine gesonderte DB für tests genutzt, und tests werden in transaktionen
 * gepackt die nach jedem tests wieder zurückgerollt werden.
 */
namespace dbaccess;

require_once('dbaccess.php');

use PHPUnit\Framework\TestCase;

/*
 * Use the test database
 *
 * All tests that hit the DB should use this helper class. (All so brittle)
 */
$GLOBALS['DBNameGuestbook'] = 'guestbookDW75';
$GLOBALS['DBHostGuestbook'] = 'db_test';

abstract class DBTransactionsTestHelper extends TestCase {

  function setUp() {
    $conn = DBAccess::getDBAccess()->getConnection();
    $result = $conn->query('start transaction');
    if ($conn->error) {die( "--------> SETUP: " . $conn->error . " <------");}
  }
  function tearDown() {
    $conn = DBAccess::getDBAccess()->getConnection();
    $result = $conn->query('rollback');
    if ($conn->error) {die( "-------> TEARDOWN: " . $conn->error . " <------");}
  }

  function executeQuery($querystring) {
    $conn = DBAccess::getDBAccess()->getConnection();
    $cursor = $conn->query($querystring);
    if ($conn->error) {die( "--------> " . $conn->error . " <------");}
    return $cursor->fetch_all(MYSQLI_ASSOC);
  }
}
