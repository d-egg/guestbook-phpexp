<?php
/*
 * formdata module
 *
 * Hier werden die Form-Daten anhand von constraints in form-tempaltes
 * ausgewertet.
 */
namespace formdata;

require_once('__init__.php');
require_once('models/constraints.php');

use Exception;
use constraints;


/*
 * Exceptions: Errors and Info
 */

class FormDataException extends Exception {}

class FormProcessor {

  // This is the field in the registration response used to convey errors that
  // occurred during the verfication.
  const SERVER_ERROR_FIELD_NAME = "_servererror";

  /*
   * Get info about the constraints that the form template applies to fields
   *
   * The return value has the same format as the form data itself, but
   * provides a complete structure for all supported fields.
   */
  function cloneTemplate($formTemplate) {
    $templateClone = [];
    foreach($formTemplate as $fieldName => $constraints){
      $fieldValue = [];
      foreach($constraints as $cKey => $cValue) {
        $fieldValue[$cKey] = clone $cValue;
      }
      $templateClone[$fieldName] = $fieldValue;
    }
    return $templateClone;
  }

  function validateFormData($formData, $formTemplate) {

    /*
     * Take a form-data array of field-names and values.  Return an array of the
     * those field-name keys if the value failed a constraint.  The value is an
     * array of failed constraints.
     *
     * There is a special server error entry for errors that cannot be
     * associated with a particular field.
     */

    $registrationErrors = [];
    $serverFails = [];

    foreach($formData as $fieldName => $fieldValue) {
      if (!array_key_exists($fieldName, $formTemplate)) {
        $sr = new constraints\ServerConstraint();
        $sr->setError("Unsupported field: $fieldName");
        $serverFails[] = $sr;
        continue;
      }
      $constraintsResponse = [];
      $fieldconstraints = $formTemplate[$fieldName];
      foreach($fieldconstraints as $constraintName => $constraintTemplate) {
        $constraint = clone $constraintTemplate;
        $constraint->verify($fieldValue);
        if ($constraint->isError()) {
          $constraintsResponse[$constraintName] = $constraint;
          if ($constraint->isFatal()) {
            break;
          }
        }
      }
      if ($constraintsResponse) {
        $registrationErrors[$fieldName] = $constraintsResponse;
      }
    }

    if ($serverFails) {
      $registrationErrors[self::SERVER_ERROR_FIELD_NAME] = $serverFails;
    }
    return $registrationErrors;
  }

}
