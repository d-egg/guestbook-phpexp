<?php
/*
 * Constraints module
 *
 * Eine Klassenhierarchie von constraints, mit der man Anforderungen an
 * Form-Daten formulieren kann.  Sie haben alle das gleiche Interface so dass
 * client code constraints abstrakt behandeln kann.
 */
namespace constraints;

require_once('__init__.php');
require_once('models/dbaccess.php');

use Exception;
use ArrayObject;
use IntlChar;

use dbaccess;

/*
 * Exceptions: Errors and Info
 */

class ConstraintsException extends Exception {}
class ConstraintsLogicException extends ConstraintsException {}


/*
 * CharDescription
 *
 * A multibyte information class to convey character information to the UI.
 */

class CharDescription {

  function __construct($charString, $isValid) {
    $this->charString = $charString;
    $this->isValid = $isValid;
  }

  // Emphasize that the charString may consist of multiple bytes
  function getByteLength() {
    return strlen($this->charString);
  }

  function getUnicodeName() {
    return IntlChar::charName($this->charString);
  }
}


/*
 * CharAnalysis
 *
 * An array of CharDescription elements where keys are the metric (first: 1)
 * positions in a given string.  A little bit like a unicode string with utf8
 * encoded bytestrings.
 */

class CharAnalysis extends ArrayObject {

  function __construct($charDescriptions) {
    parent::__construct($charDescriptions);
  }

  function isValidUsername() {
    if (!count($this)) {
      return False;
    }
    foreach ($this as $pos => $desc) {
      if (!$desc->isValid) {
        return False;
      }
    }
    return True;
  }

  function getInvalidElements() {
    return array_filter(
        $this->getArrayCopy(),
        function ($charDesc) { return !$charDesc->isValid; }
        );
  }
}


/*
 * A class hierarchy to communicate a response to the UI
 *
 * The intention is that the UI can choose what details to explore.  For example
 * a visitor pattern could be implemented as long as we create the html on the
 * server and a serialization visitor could forge a json response to support
 * client side UI.
 *
 * These classes can be used to convey information about the constraint as well
 * as record an actual violation of that constraint.  The client-side can obtain
 * a constraint description of all fields to create a registration form with
 * info fields and later gets one where the errors are filled in.
 */

abstract class Constraint {

  function __construct() {
    $this->isFatalConstraint = False;
  }

  // Query whether this constrain contains an error record
  abstract function isError();

  // Set this instance to an error condition.  The argument is supposed to
  // describe the error.
  abstract function setError($errorData);

  // Specify a value to match against the constraints and set to an error
  // condition if necessary.
  abstract function verify($subject);

  // For details that the client does not care about, create an error message
  // here.
  function getGenericErrorMessage() {
    return $this->isError() ? $this->_getGenericErrorMessage_impl() : '';
  }

  // A per instance set and query to determine whether this constraint is
  // deemed fatal to any processing.
  function isFatal() {
    return $this->isFatalConstraint;
  }
  function setFatal($wantFatal) {
    $this->isFatalConstraint = $wantFatal;
  }

  // generate the actual message in a sub-class.
  abstract function _getGenericErrorMessage_impl();
}

/*
 * Errors from the server during registration.  For example unrecognized form
 * fields.  This is mostly a dummy to fit into the constraints hierarchy.
 */
class ServerConstraint extends Constraint {

  function __construct() {
    parent::__construct();
    $this->message = NULL;
  }

  function setError($message) {
    $this->message = $message;
  }

  // Create a dummy.  Keeping all errors in the same hierarchy will allow
  // streamlined processing.
  function verify($subject) {
    throw new ConstraintsLogicException("dummy, not meant to be called");
  }

  function isError() {
    return isset($this->message);
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return $this->message;
  }
}

/*
 * Length limit violations in strings
 */
class LengthLimitsConstraint extends Constraint {

  function __construct($min, $max) {
    parent::__construct();
    $this->min = $min;
    $this->max = $max;
    $this->violation = NULL;
  }

  function setError($violation) {
    $this->violation = $violation;
  }

  function isError() {
    return isset($this->violation);
  }

  function verify($subject) {
    $this->violation = NULL;
    if ($this->min < 0 || $this->max < 0 || $this->max < $this->min) {
      throw new ConstraintsLogicException(
          "Invalid range specification.  Cannot compute.");
    }
    $stringLength = mb_strlen($subject);
    if ($stringLength < $this->min || $stringLength > $this->max) {
      $this->setError($stringLength);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Ungültige Länge $this->violation ist nicht innerhalb "
        . "der Grenzen von $this->min bis $this->max";
  }
}

/*
 * Invalid characters in a string
 */
class InvalidCharsConstraint extends Constraint {

  function __construct($allowedCharsConfig) {
    parent::__construct();
    $this->allowedCharsConfig = $allowedCharsConfig;
    $this->characterAnalysis = NULL;
  }

  function setError($characterAnalysis) {
    $this->characterAnalysis = $characterAnalysis;
  }

  function isError() {
    return isset($this->characterAnalysis);
  }

  function verify($subject) {
    $this->charAnalysis = NULL;
    $charAnalysis = $this->_getCharAnalysis($subject);
    if (!$charAnalysis->isValidUsername()) {
      $this->setError($charAnalysis);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Ungültige Zeichen an den Positionen "
      . join(", ", array_keys($this->characterAnalysis->getInvalidElements()));
  }

  function _getAllowedCharsRegex() {
    $filteredConfig = array_filter($this->allowedCharsConfig, "strlen");
    return join("|", array_values($filteredConfig));
  }

  // create a regex-alternation of the corresponding configuration
  function _isAllowedChar($subject) {
    if (mb_strlen($subject) != 1) {
      throw new ConstraintsLogicException(
        "ERROR: subject string not exactly one character '$subject'");
    }

    $usernameRegex = $this->_getAllowedCharsRegex();
    $matchOutcome = preg_match(
        '/^(' . $usernameRegex . ')$/u',
        $subject);
    if ($matchOutcome === FALSE) {
      throw new ConstraintsLogicException(
        "ERROR: pattern matching generated an error for '$subject'");
    }
    return (bool)$matchOutcome;
  }

  function _getCharAnalysis($username) {
    $unicodeChars = preg_split('//u', $username, -1, PREG_SPLIT_NO_EMPTY);
    if ($unicodeChars === FALSE) {
      throw new ConstraintsLogicException(
        "ERROR: pattern matching generated an error for '$username'");
    }
    $charDescriptions = [];
    for ($i = 0; $i < count($unicodeChars); $i++) {
      $iterChar = $unicodeChars[$i];
      $iterPos = $i + 1;
      $charDescriptions[$iterPos] = new CharDescription(
          $iterChar, $this->_isAllowedChar($iterChar));
    }
    return new CharAnalysis($charDescriptions);
  }
}

/*
 * If a string does not comply to requirements concerning the chars in it.  For
 * example in passwords.
 */
class CharDiversityConstraint extends Constraint {
  function __construct($mandatoryCharGroups) {
    parent::__construct();
    $this->mandatoryCharGroups = $mandatoryCharGroups;
    $this->missingCharGroups = NULL;
  }

  function setError($missingCharGroups) {
    // an array of strings that describe the character groups that are missing.
    $this->missingCharGroups = $missingCharGroups;
  }

  function isError() {
    return isset($this->missingCharGroups);
  }

  function verify($subject) {
    // Assemble missing char-groups in an array and return it.  A non-empty
    // return value is an error.
    $this->missingCharGroups = NULL;
    $fehlendeGruppen = [];
    // check each group
    foreach ($this->mandatoryCharGroups as $gruppe => $zeichen) {
      $kommtVor = False;
      for ($i = 0; $i < mb_strlen($zeichen); $i++) {
        $char = mb_substr($zeichen, $i, 1);
        // Abbruch für diese Gruppe wenn gefunden
        if (mb_strpos($subject, $char) !== False) {
          // diese Gruppe kommt vor.
          $kommtVor = True;
          break;
        }
      }
      // record missing group
      if (!$kommtVor) {
        $fehlendeGruppen[] = $gruppe;
      }
    }
    if ($fehlendeGruppen) {
      $this->setError($fehlendeGruppen);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Es fehlen die folgenden Zeichengruppen: "
      . join(", ", $this->missingCharGroups);
  }

}

/*
 * Invalid words in a string (case-insensitively)
 */
class InvalidWordsConstraint extends Constraint {

  function __construct($invalidWords) {
    parent::__construct();
    $this->invalidWords = $invalidWords;
    $this->wordViolations = NULL;
  }

  function setError($wordViolations) {
    $this->wordViolations = $wordViolations;
  }

  function isError() {
    return isset($this->wordViolations);
  }

  function verify($subject) {
    $this->wordViolations = NULL;
    $wordViolations = [];
    foreach($this->invalidWords as $iw) {
      if (preg_match('/' . $iw . '/i', $subject)) {
        $wordViolations[] = $iw;
      }
    }
    if (!empty($wordViolations)) {
      sort($wordViolations);
      $this->setError($wordViolations);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Es kommen verbotene Wörter vor: "
      . join(", ", $this->wordViolations);
  }
}

/*
 * Invalid email string
 */
class InvalidEmailConstraint extends Constraint {

  function __construct() {
    parent::__construct();
    $this->emailViolation = NULL;
  }

  function setError($emailViolation) {
    $this->emailViolation = $emailViolation;
  }

  function isError() {
    return isset($this->emailViolation);
  }

  function verify($subject) {
    $this->emailViolation = NULL;
    if (!filter_var($subject, FILTER_VALIDATE_EMAIL)) {
      $this->setError($subject);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Keine gültige email entsprechend php::filter_var.";
  }
}

// =========================================

/*
 * Ensure a nick does or does not exist in the DB
 */
class DBNickConstraint extends Constraint {

  function __construct($wantNickBePresent) {
    parent::__construct();
    $this->wantNickBePresent = $wantNickBePresent;
    $this->nicksPresent = NULL;
  }

  function setError($nicksPresent) {
    // the nicks present in the DB at the time of the test
    $this->nicksPresent = $nicksPresent;
  }

  function isError() {
    return isset($this->nicksPresent);
  }

  function verify($subject) {
    $this->nicksPresent = NULL;

    $db = new dbaccess\Datenbank();
    $nickExists = $db->doesNickExist($subject);
    if ($nickExists xor $this->wantNickBePresent) {
      $this->setError($db->getAllNicks());
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return sprintf("Der nick ist %s vorhanden.",
      $this->wantNickBePresent ? 'nicht' : 'bereits');
  }
}

class DBRefpostExistenceConstraint extends Constraint {

  function __construct() {
    parent::__construct();
    $this->missingRefpost = NULL;
  }

  function setError($missingRefpost) {
    // the nicks present in the DB at the time of the test
    $this->missingRefpost = $missingRefpost;
  }

  function isError() {
    return isset($this->missingRefpost);
  }

  function verify($subject) {
    $this->missingRefpost = NULL;

    $db = new dbaccess\Datenbank();
    if (!$db->doesRefpostExist($subject)) {
      $this->setError($subject);
    }
  }

  // ---

  function _getGenericErrorMessage_impl() {
    return "Die refernzierte post '$this->missingRefpost' existiert nicht.";
  }
}
