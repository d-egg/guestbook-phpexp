<?php
/*
 * dbaccess module
 *
 * Hier wird die Interaktion mit der Datenbank behandelt.
 */
namespace dbaccess;

require_once('__init__.php');

use Exception;
use mysqli;

class FieldsException extends Exception {}

/*
 * Der Name der Gästebuch Datenbank.  Global damit sie für tests geändert werden
 * kann.  (Nicht ideal).
 */
$GLOBALS['DBNameGuestbook'] = 'guestbookDW75';
$GLOBALS['DBHostGuestbook'] = 'db';

/*
 * Singleton to manage the db connection
 *
 * Erstelle und initialisiere die Datenbank wenn sie noch nicht existiert.
 */
class DBAccess {

  const USER = 'guestbook';
  const PASSWORT = 'guestbook';

  const INIT_DB_FILE = 'guestbookdb.sql';

  protected static $_singleton = NULL;

  protected function __construct() {
    $this->__is = uniqid();
    $this->DB = $GLOBALS['DBNameGuestbook'];
    $this->HOST = $GLOBALS['DBHostGuestbook'];
    $this->conn = new mysqli($this->HOST, self::USER, self::PASSWORT);
    if ($this->conn->error) {
      die( "--------> " . $this->conn->error . " <------");
    }
    $this->_setCharset();
    if (!$this->_init_doesDBExist()) {
      $this->_init_putDBIntoPristineState();
    } else {
      $this->_query_die('USE ' . $this->DB);
    }
  }

  function _query_die($querystring) {
    $cursor = $this->conn->query($querystring);
    if ($this->conn->error) {
      die( "--------> " . $this->conn->error . " <------");
    }
    return $cursor;
  }

  function _setCharset() {
    if (!$this->conn->set_charset(ENCODING_GUESTBOOK_DB)) {
      die("Konnte das encoding für die DB nicht setzen ("
          . ENCODING_GUESTBOOK_DB . "): "
          . $this->conn->error);
    }
  }

  function _init_doesDBExist() {
    $result = $this->conn->query("SHOW DATABASES LIKE '" . $this->DB . "';");
    if ($this->conn->error) {die( "-----> " . $this->conn->error . " <------");}
    if ($result->num_rows == 1) {
      return True;
    }
    return False;
  }

  function _init_putDBIntoPristineState() {
    $this->_query_die('DROP DATABASE IF EXISTS ' . $this->DB);
    $this->_query_die('CREATE DATABASE ' . $this->DB);
    $this->_query_die('USE ' . $this->DB);

    $dir_path = dirname(realpath(__FILE__), 1);
    $file_path = $dir_path . DIRECTORY_SEPARATOR . self::INIT_DB_FILE;
    $db_init_code = file_get_contents($file_path);
    if ($db_init_code === False) {
      throw new FieldsException("could not get contents of $file_path");
    }
    $result = $this->conn->multi_query($db_init_code);
    if ($this->conn->error) {die( "-----> " . $this->conn->error . " <------");}
    // you need to pick up all results of a multi-query.
    do {
      $this->conn->use_result();
    } while( $this->conn->more_results() && $this->conn->next_result() );
  }

  static function getDBAccess() {
    if (is_null(self::$_singleton)) {
      self::$_singleton = new DBAccess();
    }
    return self::$_singleton;
  }

  function getConnection() {
    return $this->conn;
  }
}

/*
 * Datenbank Zugriff
 *
 * Diverse Hilfsfunktionen um mit der Datenbank zu interagieren.
 */
class Datenbank {

  const TABLE_USERS = 'nutzer';
  const TABLE_POSTS = 'posts';
  const TABLE_COMMENTS = 'kommentare';

  function __construct() {
    $this->connection = DBAccess::getDBAccess()->getConnection();
  }

  /*
   * Query wrapper to test for error conditions
   */
  function _query_die($querystring) {
    $cursor = $this->connection->query($querystring);
    if ($this->connection->error) {
      die( "--------> " . $this->connection->error . " <------");
    }
    return $cursor;
  }

  /*
   * === Messages ===
   */

  function doesRefpostExist($refpost) {
    $querystring = sprintf(
        "SELECT id from " . self::TABLE_POSTS . " WHERE id = '%s'",
        $this->connection->escape_string(trim($refpost)));

    $cursor = $this->_query_die($querystring);
    return $cursor->num_rows > 0 ? True : False;
  }

  /*
   * Get a range of messages.  Either posts (refpost=NULL) or comments.
   */
  function getMessageRange($skip, $count, $refpost=NULL) {

    $table = self::TABLE_POSTS;
    $where_clause = '';
    if (!is_null($refpost)) {
      $table = self::TABLE_COMMENTS;
      $where_clause = ' WHERE refpost =  '. $refpost;
    }

    $querystring = "SELECT * FROM $table $where_clause "
        . " ORDER BY datum DESC "
        . " LIMIT $skip , $count";
    $cursor = $this->_query_die($querystring);
    return $cursor->fetch_all(MYSQLI_ASSOC);
  }

  function getParticularPost($postId) {
    $querystring = "SELECT * from " . self::TABLE_POSTS
        . " WHERE id = $postId";
    $cursor = $this->_query_die($querystring);
    return $cursor->fetch_assoc();
  }

  /*
   * Store a new message in the database.  Either a post ($refpost=NULL) or
   * a new comment
   */
  function storeMessage($msg, $user, $refpost) {
    $table = self::TABLE_POSTS;
    $key_ext = '';
    $val_ext = '';
    if (!is_null($refpost)) {
      $table = self::TABLE_COMMENTS;
      $key_ext = ', refpost';
      $val_ext = ', ' . $refpost;
    }
    $querystring = sprintf(
        "INSERT INTO $table (message, datum, nutzer $key_ext ) "
        . " VALUES ('%s', NOW(), '%s' $val_ext )",
        $this->connection->escape_string($msg),
        $this->connection->escape_string($user));

    $this->_query_die($querystring);
  }

  /*
   * number of messages
   */

  function getNumberOfComments($refpost) {
    $querystring = "SELECT * from " . self::TABLE_COMMENTS
        . " WHERE refpost = '$refpost'";
    $cursor = $this->_query_die($querystring);
    return $cursor->num_rows;
  }

  function getNumberOfPosts() {
    $querystring = "SELECT * from " . self::TABLE_POSTS;
    $cursor = $this->_query_die($querystring);
    return $cursor->num_rows;
  }

  /*
   * user data
   */

  function getUserData($nick) {
    $querystring = "SELECT * from " . self::TABLE_USERS
        . " WHERE nick = '$nick'";
    $cursor = $this->_query_die($querystring);

    $num_rows = $cursor->num_rows;
    if ($num_rows > 1) {
      die("Unexpected: more than one user '$nick'");
    } elseif ($num_rows == 0) {
      return null;
    } else {
      return $cursor->fetch_assoc();
    }
  }

  function getAllNicks() {
    $querystring = "SELECT nick from " . self::TABLE_USERS
        . " ORDER BY nick";

    $cursor = $this->_query_die($querystring);

    return array_reduce(
      $cursor->fetch_all(),
      function ($carry, $item) { $carry[] = $item[0]; return $carry; },
      []);
  }

  function doesNickExist($nick_evil) {
    $querystring = sprintf(
        "SELECT nick from " . self::TABLE_USERS . " WHERE nick = '%s'",
        $this->connection->escape_string(trim($nick_evil)));

    $cursor = $this->_query_die($querystring);

    return $cursor->num_rows > 0 ? True : False;
  }

  function storeNewUser($nick, $password, $vorname, $nachname, $email) {
    $querystring = sprintf("INSERT INTO " . self::TABLE_USERS
        . " (nick, password, vorname, nachname, email) VALUES "
        . " ('%s', '%s', '%s', '%s', '%s')",
        $this->connection->escape_string($nick),
        $this->connection->escape_string($password),
        $this->connection->escape_string($vorname),
        $this->connection->escape_string($nachname),
        $this->connection->escape_string($email));
    $result = $this->_query_die($querystring);
  }

  function getPasswordHash($nick_evil) {
    $querystring = sprintf(
        "SELECT password FROM " . self::TABLE_USERS
        . " WHERE nick = '%s'",
        $this->connection->escape_string(trim($nick_evil)));
    $cursor = $this->_query_die($querystring);
    $result = $cursor->fetch_assoc();
    if (is_null($result)) {
      return null;
    } else {
      return $result['password'];
    }
  }
}
