<?php
namespace messages;

require_once('messages.php');
require_once('models/constraints.php');
require_once('models/dbtesthelper.php');

use PHPUnit\Framework\TestCase;

class Messages_getNumberOfPostPages extends TestCase {

  function testNumberOfPostPages_UnevenSplit() {
    $posts = new Posts();
    $this->assertSame(2, $posts->getNumberOfPostPages('2'));
    $this->assertSame(1, $posts->getNumberOfPostPages('200'));
  }

  function testNumberOfPostPages_ExactSplit() {
    $posts = new Posts();
    $this->assertSame(1, $posts->getNumberOfPostPages('3'));
  }

  function testNumberOfPostPages_SinglePageFallback() {
    $posts = new Posts();
    $this->assertSame(1, $posts->getNumberOfPostPages('0'));
  }
}

class Messages_getNumberOfCommentPages extends TestCase {

  function testNumberOfCommentsPages() {
    $posts = new Posts();
    $this->assertSame(4, $posts->getNumberOfCommentsPages('1', '1'));
    $this->assertSame(2, $posts->getNumberOfCommentsPages('2', '1'));
    $this->assertSame(2, $posts->getNumberOfCommentsPages('3', '1'));
    $this->assertSame(1, $posts->getNumberOfCommentsPages('4', '1'));
  }

  function testNumberOfCommentsPages_SinglePageFallback() {
    $posts = new Posts();
    $this->assertSame(1, $posts->getNumberOfCommentsPages('0', '1'));
  }
}

class Messages_isGoodNumberString extends TestCase {

  function testValidNumbers() {
    $posts = new Posts();
    $this->assertTrue($posts->_isGoodNumberString('24'));
    $this->assertTrue($posts->_isGoodNumberString('01234567890123456789'));
  }

  function testInvalidNumbers() {
    $posts = new Posts();
    $this->assertFalse($posts->_isGoodNumberString('2x4'));
    $this->assertFalse($posts->_isGoodNumberString('24x>%'));
    $this->assertFalse($posts->_isGoodNumberString('%'));
  }
}

class Messages_stringToIntChecked extends TestCase {

  function testValidConversion() {
    $posts = new Posts();
    $this->assertSame(24, $posts->_stringToIntChecked('24', 'test'));
  }

  /**
   * @expectedException messages\MessagesException
   */
  function testInvalidConversion() {
    $posts = new Posts();
    $this->assertSame(24, $posts->_stringToIntChecked('2x4', 'test'));
  }
}

class Messages_getPostsForPage extends TestCase {

  function testGetOneParticularPost() {
    $pc = new Posts();
    $results = $pc->getPostsForPage('1', '2');
    $this->assertEquals(1, count($results));
    $this->assertEquals(3, $results[0]['id']);
  }

  function testGetPostsPageBeyondTheEnd() {
    $pc = new Posts();
    $results = $pc->getPostsForPage('1', '55');
    $this->assertEquals(0, count($results));
  }
}

class Messages_getCommentsForPage extends TestCase {

  function testGetOneParticularComment() {
    $pc = new Posts();
    $results = $pc->getCommentsForPage('2', '2', '1');
    $this->assertEquals(2, count($results));
    $this->assertEquals(4, $results[0]['id']);
  }

  function testGetCommentsPageBeyondTheEnd() {
    $pc = new Posts();
    $results = $pc->getCommentsForPage('1', '55', '1');
    $this->assertEquals(0, count($results));
  }
}

class Messages_storeNewPost extends \dbaccess\DBTransactionsTestHelper {

  function testStoreValidPost() {
    $pc = new Posts();
    $result = $pc->storeNewPost([
        'message' => 'time goes on and on',
        'user' => 'sweeper',
    ]);
    $this->assertEmpty($result);

    $results = $this->executeQuery(
        "select * from posts where message = 'time goes on and on'");
    $this->assertEquals(1, count($results));
    $this->assertEquals('sweeper', $results[0]['nutzer']);
  }

  function testBreakConstraintsWhileTryingToStoreNewPost() {
    $pc = new Posts();
    $result = $pc->storeNewPost([
        'message' => 'fuck time',
        'user' => 'broom',
    ]);
    $this->assertEquals(2, count($result));
    $this->assertRegExp('/fuck/',
        $result['message']['swear']->getGenericErrorMessage());
    $this->assertRegExp('/nick/',
        $result['user']['presence']->getGenericErrorMessage());

    $results = $this->executeQuery(
        "select * from posts where message = 'fuck time'");
    $this->assertEquals(0, count($results));
  }
}

class Messages_storeNewComment extends \dbaccess\DBTransactionsTestHelper {

  function testStoreValidComment() {
    $pc = new Posts();
    $result = $pc->storeNewComment([
        'message' => 'time is over',
        'user' => 'sweeper',
        'refpost' => '1',
    ]);
    $this->assertEmpty($result);

    $results = $this->executeQuery(
        "select * from kommentare where message = 'time is over'");
    $this->assertEquals(1, count($results));
    $this->assertEquals('sweeper', $results[0]['nutzer']);
  }

  function testBreakConstraintsWhileTryingToStoreNewComment() {
    $pc = new Posts();
    $result = $pc->storeNewComment([
        'message' => 'fuck time',
        'user' => 'broom',
        'refpost' => '12345678',
    ]);
    $this->assertEquals(3, count($result));
    $this->assertRegExp('/fuck/',
        $result['message']['swear']->getGenericErrorMessage());
    $this->assertRegExp('/nick/',
        $result['user']['presence']->getGenericErrorMessage());
    $this->assertRegExp('/12345678/',
        $result['refpost']['existence']->getGenericErrorMessage());

    $results = $this->executeQuery(
        "select * from kommentare where message = 'fuck time'");
    $this->assertEquals(0, count($results));
  }
}
