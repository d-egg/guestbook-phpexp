<?php
/*
 * messages module
 *
 * Behandelt alles was mit message browsing zu tun hat.  Die
 * Anforderungen an Form-Daten werden mit constraints konfiguriert.
 */
namespace messages;

require_once('__init__.php');
require_once('models/constraints.php');
require_once('models/dbaccess.php');
require_once('models/formdata.php');

use Exception;
use constraints;
use dbaccess;
use formdata;

class MessagesException extends Exception {}

/*
 * This is the facade for message based UI sites
 */
class Posts {

  protected $forbiddenWords = ['damn', 'shit', 'mist', 'klomensch', 'fuck',
      'dungesser', 'vollpfosten'];

  function __construct() {
    $this->db = new dbaccess\Datenbank();
    $this->formProc = new formdata\FormProcessor();

    $this->numbersConstraint = new constraints\InvalidCharsConstraint(
        [ "number" => "[0123456789]" ]);
    $this->postTemplate = $this->_getMessageTemplate(False);
    $this->commentTemplate = $this->_getMessageTemplate(True);
  }

  // fixme: only numbers coming in now.  Remove the checks?

  // 0 argument to put all on a single page
  function getNumberOfPostPages($postsPerPageCount_str) {
    $pppCount = $this->_stringToIntChecked($postsPerPageCount_str, __METHOD__);
    if ($pppCount <= 0) {
      return 1;
    }
    $allPostCount = $this->db->getNumberOfPosts();
    return (int)ceil((float)$allPostCount / (float)$pppCount);
  }

  function getNumberOfCommentsPages($commentsPerPageCnt_str, $refpost_str) {
    $refpost = $this->_stringToIntChecked($refpost_str, __METHOD__);
    $cppCount = $this->_stringToIntChecked($commentsPerPageCnt_str, __METHOD__);

    if ($cppCount <= 0) {
      return 1;
    }
    $allPostCount = $this->db->getNumberOfComments($refpost);
    return (int)ceil((float)$allPostCount / (float)$cppCount);
  }

  function getReferencedPost($refpost) {
    $pppCount = $this->_stringToIntChecked($refpost, __METHOD__);
    return $this->db->getParticularPost($refpost);
  }

  // ---

  function getPostsForPage($postsPerPageCount_str, $pageNumber_str) {
    return $this->_getMessagesForPage($postsPerPageCount_str, $pageNumber_str,
        null);
  }

  function getCommentsForPage($cppCount_str, $pageNumber_str, $refpost_str) {
    $refpost = $this->_stringToIntChecked($refpost_str, __METHOD__);
    return $this->_getMessagesForPage($cppCount_str, $pageNumber_str,
        $refpost);
  }

  // ---

  function storeNewPost($formData) {
    $formErrors = $this->formProc->validateFormData(
        $formData, $this->postTemplate);

    if (!empty($formErrors)) {
      return $formErrors;
    }

    $this->db->storeMessage(
        $formData['message'],
        $formData['user'],
        NULL);
    return [];
  }

  function storeNewComment($formData) {
    $formErrors = $this->formProc->validateFormData(
        $formData, $this->commentTemplate);

    if (!empty($formErrors)) {
      return $formErrors;
    }

    $this->db->storeMessage(
        $formData['message'],
        $formData['user'],
        $formData['refpost']);
    return [];
  }

  // =============================

  function _getMessagesForPage($pppCount_str, $pageNumber_str, $refpost) {
    $pppCount = $this->_stringToIntChecked($pppCount_str, __METHOD__);
    $pageNumber = $this->_stringToIntChecked($pageNumber_str, __METHOD__);

    if ($pageNumber <= 0) {
      $pageNumber = 1;
    }
    $skip = $pppCount * ($pageNumber - 1);
    $requestedPosts = $this->db->getMessageRange($skip, $pppCount, $refpost);
    return $requestedPosts;
  }

  function _isGoodNumberString($str) {
    $constraint = clone $this->numbersConstraint;
    $constraint->verify($str);
    if ($constraint->isError()) {
      return False;
    }
    return True;
  }

  function _stringToIntChecked($str, $origin) {
    if (!$this->_isGoodNumberString($str)) {
      throw new MessagesException("not a number 'str' from"
          . " from origin '$origin'");
    }
    return (int)$str;
  }

  function _getMessageTemplate($wantCommentTemplate) {

    // message
    $messageLenC = new constraints\LengthLimitsConstraint(1, 1000);
    $messageLenC->setFatal(True);
    $messageWordsC = new constraints\InvalidWordsConstraint(
        $this->forbiddenWords);

    $messageField['length'] = $messageLenC;
    $messageField['swear'] = $messageWordsC;

    // user
    $nickLenC = new constraints\LengthLimitsConstraint(1, 30);
    $nickLenC->setFatal(True);
    $nickExistsC = new constraints\DBNickConstraint(True);

    $userField['length'] = $nickLenC;
    $userField['presence'] = $nickExistsC;

    // refpost
    $refpostLenC = new constraints\LengthLimitsConstraint(1, 1000);
    $refpostLenC->setFatal(True);
    $refpostC = new constraints\DBRefpostExistenceConstraint();

    $refpostField['length'] = $refpostLenC;
    $refpostField['existence'] = $refpostC;

    // registry template
    $messageTemplate = [];
    $messageTemplate['message'] = $messageField;
    $messageTemplate['user'] = $userField;
    if ($wantCommentTemplate) {
      $messageTemplate['refpost'] = $refpostField;
    }

    return $messageTemplate;
  }
}
