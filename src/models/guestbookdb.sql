SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';

CREATE TABLE nutzer (
  nick varchar(30) PRIMARY KEY,
  password varchar(255) not null,
  vorname varchar(100) not null,
  nachname varchar(100) not null,
  email varchar(100) not null
);

CREATE TABLE posts (
  id Int PRIMARY KEY AUTO_INCREMENT,
  message varchar(1000) not null,
  datum Datetime not null,
  nutzer varchar(30) not null,

    CONSTRAINT fkpost_nutzer_nick
    FOREIGN KEY (nutzer)
    REFERENCES nutzer(nick)
    ON UPDATE CASCADE
);

CREATE TABLE kommentare (
  id Int PRIMARY KEY AUTO_INCREMENT,
  message varchar(1000) not null,
  datum Datetime not null,
  nutzer varchar(30) not null,
  refpost Int not null,

    CONSTRAINT fk_nutzer_nick
    FOREIGN KEY (nutzer)
    REFERENCES nutzer(nick)
    ON UPDATE CASCADE,

    CONSTRAINT fkkommentare_post_id
    FOREIGN KEY (refpost)
    REFERENCES posts(id)
);

describe nutzer;
describe posts;
describe kommentare;

INSERT INTO nutzer VALUES
  /* dejaFu%7 */
  ('sweeper', '$2y$10$Ni6x.d3PfnWIiLx0R0VgqOCkZMi2pMZoeZQDWXzaWD7rujBIFvxNG',
      'Lu', 'Tse', 'sweeper@hmonks.org'),
  /* suSan%77 */
  ('susan', '$2y$10$VDik3p429M9j37wJgjDPF.qweqixA0jddTSflhBWUEQcl7EJop3Yi',
      'Susan', 'Sto-Helit', 'susan@orchid.net'),
  ('Anonymous', 'nologin', 'Anon', 'Ymous', 'anon@example.tld');

INSERT INTO posts VALUES
  (1, 'Zeit ist ein Konstrukt',
      '2017-06-21 15:00:00', 'sweeper'),
  (3, 'Jetzt reden die im anderen thread schon wieder über Zeit (nerf!)',
      '2017-06-21 15:11:01', 'Anonymous'),
  (2, 'Einsatz! Zu den Ebern!',
      '2017-06-22 13:01:21', 'susan');

INSERT INTO kommentare VALUES
  (1, 'Ja, aber der Ablauf der Dinge insgesamt ist real.',
      '2017-06-21 15:10:00', 'Anonymous', 1),
  (4, 'Ich habe keine Zeit.',
      '2017-06-21 15:20:00', 'Anonymous', 1),
  (3, 'Ist doch genug für alle da.',
      '2017-06-21 15:30:00', 'susan', 1),
  (2, '@anon: Ich mach dir was klar',
      '2017-06-21 15:40:00', 'sweeper', 1),
  (5, 'Komm zu uns in den Materie club (@mattermatters)',
      '2017-06-22 16:40:00', 'Anonymous', 3),
  (6, 'Sauber, danke.',
      '2017-06-22 16:45:00', 'Anonymous', 3),
  (7, 'Bin dabei.',
      '2017-06-23 17:25:10', 'sweeper', 2);

select * from nutzer;
select * from posts;
select * from kommentare;
