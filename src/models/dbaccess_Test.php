<?php
namespace dbaccess;

require_once('dbaccess.php');
require_once('dbtesthelper.php');

use PHPUnit\Framework\TestCase;

class DBAccess_Tests extends DBTransactionsTestHelper {

  /*
   * dbaccess
   */

  function testSingletonMultipleInstancesAreTheSame() {
    $itNow = DBAccess::getDBAccess();
    $itThen = DBAccess::getDBAccess();
    $this->assertEquals($itNow->__is, $itThen->__is);
  }

  function testIsTestDatabase() {
    $dbaccess = DBAccess::getDBAccess();
    $this->assertEquals($dbaccess->DB, 'guestbookDW75');
  }

  function testCreateAnotherTestTable() {
    $conn = DBAccess::getDBAccess()->getConnection();

    $result = $conn->query('DROP TABLE IF EXISTS tt2');
    $this->assertTrue($result);

    $result = $conn->query('CREATE TABLE tt2 (col1 Int(10), col2 char(2))');
    $this->assertTrue($result);

    $result = $conn->query('DROP TABLE IF EXISTS tt2');
    $this->assertTrue($result);
  }

  function testDBExistenceDeterminedCorrectly() {
    $dbaccess = DBAccess::getDBAccess();
    $conn = $dbaccess->getConnection();

    $result = $conn->query("create database if not exists "
        . $dbaccess->DB ."");
    $this->assertTrue($dbaccess->_init_doesDBExist());
    // todo custom assertion for DDL
    $result = $conn->query("drop database if exists " . $dbaccess->DB . "");
    if ($conn->error) {$this->fail( "--------> " . $conn->error . " <------");}
    $this->assertFalse($dbaccess->_init_doesDBExist());
    $dbaccess->_init_putDBIntoPristineState();
  }

  function testDbInitWorks() {
    $dbaccess = DBAccess::getDBAccess();
    $conn = $dbaccess->getConnection();

    $result = $conn->query("drop database if exists " . $dbaccess->DB . "");
    if ($conn->error) {$this->fail( "--------> " . $conn->error . " <------");}
    $this->assertFalse($dbaccess->_init_doesDBExist());

    // todo check for _any_ errors every time
    $dbaccess->_init_putDBIntoPristineState();
    if ($conn->error) {$this->fail( "--------> " . $conn->error . " <------");}
    $this->assertTrue($dbaccess->_init_doesDBExist());
  }
}

class Datenbank_Tests extends DBTransactionsTestHelper {

  /*
   * messages
   */

  function testDoesRefpostExist_Benign() {
    $db = new Datenbank();
    $this->assertTrue($db->doesRefpostExist('1'));
    $this->assertTrue($db->doesRefpostExist(' 2'));
    $this->assertTrue($db->doesRefpostExist('3 '));

    $this->assertFalse($db->doesRefpostExist('broom'));
  }

  function testDoesRefpostExist_Malicious() {
    $db = new Datenbank();
    $this->assertFalse($db->doesRefpostExist("m'; drop table posts;comment '"));
    $this->assertTrue($db->doesRefpostExist('1'));
  }

  function testGetARangeOfPosts() {
    $db = new Datenbank();
    $result = $db->getMessageRange(1, 1);
    $this->assertEquals(3, $result[0]['id']);
  }

  function testGetARangeOfComments() {
    $db = new Datenbank();
    $result = $db->getMessageRange(1, 2, 1);
    $this->assertEquals(2, count($result));
    $this->assertEquals('Ist doch genug für alle da.', $result[0]['message']);
    $this->assertEquals(4, $result[1]['id']);
  }

  function testInsertAComment() {
    $db = new Datenbank();
    $conn = $db->connection;
    $db->storeMessage("testMsg234", "sweeper", 2);

    $cursor = $conn->query(
        "select * from kommentare where message = 'testMsg234'");
    if ($conn->error) {$this->fail( "--------> " . $conn->error . " <------");}
    $this->assertEquals(1, $cursor->num_rows);
    $result = $cursor->fetch_assoc();
    $this->assertEquals('sweeper', $result['nutzer']);
  }

  function testNumberOfComments() {
    $db = new Datenbank();
    $this->assertEquals(4, $db->getNumberOfComments(1));
  }

  function testNumberOfPosts() {
    $db = new Datenbank();
    $this->assertEquals(3, $db->getNumberOfPosts());
  }

  /*
   * user data
   */

  function testGetAValidUser() {
    $db = new Datenbank();
    $userdata = $db->getUserData('sweeper');
    $this->assertEquals('Tse', $userdata['nachname']);
  }

  function testGetAInvalidUser() {
    $db = new Datenbank();
    $userdata = $db->getUserData('notpresent');
    $this->assertEquals(null, $userdata);
  }

  function testGetAllNicks() {
    $db = new Datenbank();
    $this->assertEquals(['Anonymous', 'susan', 'sweeper'], $db->getAllNicks());
  }

  function testDoesNickExist_Benign() {
    $db = new Datenbank();
    $this->assertTrue($db->doesNickExist('susan'));
    $this->assertTrue($db->doesNickExist('sweeper'));
    $this->assertTrue($db->doesNickExist(' sweeper'));
    $this->assertTrue($db->doesNickExist('sweeper '));

    $this->assertFalse($db->doesNickExist('broom'));
  }

  function testDoesNickExist_Malicious() {
    $db = new Datenbank();
    $this->assertFalse($db->doesNickExist("m'; drop table nutzer; comment '"));
    $this->assertTrue($db->doesNickExist('susan'));
  }

  function testStoreANewUser() {
    $db = new Datenbank();
    $conn = $db->connection;
    $db->storeNewUser('testnick', 'PW', 'VN', 'NN', 'EMAIL');

    $cursor = $conn->query(
        "select * from nutzer where nick = 'testnick'");
    if ($conn->error) {$this->fail( "--------> " . $conn->error . " <------");}
    $this->assertEquals(1, $cursor->num_rows);
    $result = $cursor->fetch_assoc();
    $this->assertEquals('PW', $result['password']);
  }
}
