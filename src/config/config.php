<?php
/*
 * Allgemeine Konfiguration
 */

// Encoding auf UTF-8 festlegen.
// print join(mb_list_encodings(), PHP_EOL);
const ENCODING_GUESTBOOK_DB = 'utf8';
const ENCODING_GUESTBOOK = 'UTF-8';
mb_internal_encoding(ENCODING_GUESTBOOK);
mb_regex_encoding(ENCODING_GUESTBOOK);

/*
 * Set the include path for our project
 */
class GuestbookPathAdaption {

  function getProjectRoot() {
    return realpath(dirname(__FILE__, 2));
  }

  function adaptIncludePath($includePath, $wantCwdPrefix) {
    $projectRoot = $this->getProjectRoot();
    $pathComponents = explode(PATH_SEPARATOR, $includePath);
    $pathComponents = array_filter($pathComponents,
        function($e) use ($projectRoot) {
            return $e != '.' && $e != $projectRoot; });
    array_unshift($pathComponents, $this->getProjectRoot());
    if ($wantCwdPrefix) {
      array_unshift($pathComponents, '.');
    }
    return implode(PATH_SEPARATOR, $pathComponents);
  }
}

set_include_path((new GuestbookPathAdaption)->adaptIncludePath(
    get_include_path(), False));


// Global config file
