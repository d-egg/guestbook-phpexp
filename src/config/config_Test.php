<?php

require_once('config.php');

use PHPUnit\Framework\TestCase;

/**
 * @requires OS Linux
 */
class IncludePathAdaptionTest extends TestCase {

  function testAdaptPathPlusCWD() {
    $pa = new class() extends GuestbookPathAdaption{
      function getProjectRoot() { return '/any/old/path'; }
    };
    $this->assertEquals('.:/any/old/path:a:b:c',
        $pa->adaptIncludePath('a:.:b:c', True));
  }

  function testAdaptPathWithoutCWD() {
    $pa = new class() extends GuestbookPathAdaption{
      function getProjectRoot() { return '/any/old/path'; }
    };
    $this->assertEquals('/any/old/path:a:b:c',
        $pa->adaptIncludePath('a:.:b:c', False));
  }

  function testAdaptPathIsIdempotent() {
    $pr = '/any/old/path';
    $pa = new class($pr) extends GuestbookPathAdaption{
      function __construct($pr) { $this->pr = $pr; }
      function getProjectRoot() { return $this->pr; }
    };
    $expected = '.:' . $pr . ':a:b';
    $this->assertEquals($expected, $pa->adaptIncludePath('a:.:b:' . $pr, True));
    $this->assertEquals($expected, $pa->adaptIncludePath($expected, True));
  }
}
