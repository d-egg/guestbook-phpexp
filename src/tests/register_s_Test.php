<?php
/*
 * System tests for register.php
 */
namespace register;

require_once('__init__.php');
require_once('models/register.php');

use PHPUnit\Framework\TestCase;

/*
 * UserRegistry Fake
 *
 * Control the configuration for system tests
 */

class UserRegistryTestConfig extends UserRegistry {

  // control the config
  protected $mandatoryCharGroups = [
    "lower" => "abcde",
    "special" => "$%&",
  ];

  protected $minMaxLengths = [
    self::PROFILETRAIT_USERNAME => ['min' => 4, 'max' => 30],
    self::PROFILETRAIT_PASSWORD => ['min' => 4, 'max' => 30],
  ];

  // map descriptive terms to single-character-match regular-expressions.
  protected $allowedUsernameCharGroups = [
      "letters" => "\p{L}",
      "number" => "\p{N}",
  ];
}

/*
 * CharDescription Tests
 */

class System_registerNewUser extends TestCase {

  function assertRegistrationErrors($formData, $expectedNumErrors) {
    $ur = new UserRegistryTestConfig();
    $registrationError = $ur->validateRegistrationData($formData);
    $numberOfErrorStrings = 0;
    $allErrorStrings = [];
    foreach($registrationError as $fieldName => $failedConstraints) {
      foreach($failedConstraints as $constraint) {
        $errorMessage = $constraint->getGenericErrorMessage();
        $this->assertGreaterThan(5, strlen($errorMessage));
        $allErrorStrings[] = $errorMessage;
        $numberOfErrorStrings++;
      }
    }
    if ($expectedNumErrors != $numberOfErrorStrings) {
      $this->assertEquals([], $allErrorStrings,
        "Custom Assertion: number of errors not equal to $expectedNumErrors");
    }
    // quiet risky warning for now.  Figure out!
    $this->assertEquals($expectedNumErrors, $numberOfErrorStrings);
  }

  /*
   * ---
   */

  function testUsernameLengthViolationIsFatal() {

    $formData = [
      UserRegistry::PROFILETRAIT_USERNAME => "i%",
    ];

    $this->assertRegistrationErrors($formData, 1);
  }

  function testPasswordLengthViolationIsFatal() {

    $formData = [
      UserRegistry::PROFILETRAIT_PASSWORD => "ia",
    ];

    $this->assertRegistrationErrors($formData, 1);
  }

  /*
   * ---
   */

  function testAllFieldsInvalid() {

    $formData = [
      UserRegistry::PROFILETRAIT_USERNAME => "invalid%&user",
      UserRegistry::PROFILETRAIT_PASSWORD => "toosimplistic",
    ];

    $this->assertRegistrationErrors($formData, 2);
  }

  function testAllFieldsGood() {

    $formData = [
      UserRegistry::PROFILETRAIT_USERNAME => "validuser",
      UserRegistry::PROFILETRAIT_PASSWORD => "abc%bc",
    ];

    $this->assertRegistrationErrors($formData, 0);
  }
}

class System_getFieldInfo extends TestCase {
  function testResponseStructureMatchesTemplate() {
    $ur = new UserRegistry();
    $fieldTemplate = $ur->registryTemplate;
    $fieldInfo = $ur->getFieldInfo();
    $this->assertEquals(array_keys($fieldInfo), array_keys($fieldTemplate));
    foreach(array_keys($fieldInfo) as $fieldName) {
      $this->assertEquals(
          array_keys($fieldInfo[$fieldName]),
          array_keys($fieldTemplate[$fieldName]));
      foreach(array_keys($fieldInfo[$fieldName]) as $constraintName) {
        $this->assertEquals(
            isset($constraintName, $fieldInfo[$fieldName]),
            isset($constraintName, $fieldTemplate[$fieldName]));
      }
    }
  }
}
