-include Makefile.local

DC_SYSTEM_CMD = docker-compose -f docker-compose.yml -f docker-compose-test.yml
DC_EXEC_GB_CMD = docker-compose exec guestbook

.DEFAULT_GOAL = build

# --- Build

build:
	${DC_SYSTEM_CMD} build
up: build
	${DC_SYSTEM_CMD} up
down:
	${DC_SYSTEM_CMD} down

shell:
	${DC_EXEC_GB_CMD} bash

test:
	${DC_SYSTEM_CMD} run phptest
