#!/bin/bash

set -e

ep_log() {
    echo "-----> entrypoint: $@"
}

ep_log "going to wait for db..."
while ! mysqladmin ping --silent \
            --host=$EP_DB_HOST \
            --user=root \
            --password=$MYSQL_ROOT_PASSWORD ; do
    echo "waiting for db..."
    sleep 2
done

ep_log "Starting: exec with: $@"
exec "$@"
